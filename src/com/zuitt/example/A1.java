package com.zuitt.example;

import java.util.Scanner;

public class A1 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");
        try {
            Scanner input = new Scanner(System.in);
            int number = input.nextInt();
            int answer = 1;
            int counter = 1;

            if (number == 0 || number == 1) {
                answer = 1;
            }
            while (counter <= number) {
                answer *= counter;
                counter++;
            }
            System.out.println("The factorial of " + number + " is " + answer);
        }
        catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
    }
}
